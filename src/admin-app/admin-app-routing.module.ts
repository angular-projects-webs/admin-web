import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAppRouterOutLetComponent } from './admin-app-router-outlet-component';
import { ProjectsModule } from 'src/projects/projects.module';
import { BaseDataManagementComponent } from 'src/projects/Base-data-management/Base-data-management.component';
import { MenuManagementComponent } from 'src/projects/menu-management/menu-management.component';
import { PageApiComponent } from 'src/projects/page-api/page-api.component';
import { ProgramListComponent } from 'src/projects/program-list/program-list.component';
import { RoleManagementComponent } from 'src/projects/role-management/role-management.component';
import { UserManagementComponent } from 'src/projects/user-management/user-management.component';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminAppRouterOutLetComponent,
    children: [
      {
        path: 'base-data-management',
        component: BaseDataManagementComponent,
      },
      {
        path: 'menu-management',
        component: MenuManagementComponent,
      },
      {
        path: 'program-list',
        component: ProgramListComponent,
      },
      {
        path: 'Page-api',
        component: PageApiComponent,
      },
      {
        path: 'role-management',
        component: RoleManagementComponent,
      },
      {
        path: 'user-management',
        component: UserManagementComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    ProjectsModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
