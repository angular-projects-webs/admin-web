import { Component } from '@angular/core';

@Component({
  selector: 'admin-app-root',
  templateUrl: './admin-app.component.html',
  styleUrls: ['./admin-app.component.scss']
})
export class AdminAppComponent {
  title = 'admin-web';
}
