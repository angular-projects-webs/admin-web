import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AdminAppRouterOutLetComponent } from './admin-app-router-outlet-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './admin-app-routing.module';
import { AdminAppComponent } from './admin-app.component';

import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AdminAppComponent,
    AdminAppRouterOutLetComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AdminAppComponent]
})
export class AdminAppModule { }
