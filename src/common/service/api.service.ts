// HTTP 客户端服务提供了以下主要功能。

// 请求类型化响应对象的能力。

// 简化的错误处理。

// 各种特性的可测试性。

// 请求和响应的拦截机制。
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { observable, Observable } from 'rxjs';

export interface Config {
  heroesUrl: string;
  textfile: string;
  date: any;
}

export interface Option {
  headers?: HttpHeaders | { [header: string]: string | string[] };
  observe?: 'body' | 'events' | 'response';
  params?: HttpParams | { [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean> };
  reportProgress?: boolean;
  responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {

  }

  get<T>(): Observable<any> {
    // now returns an Observable of Config
    const httpClient = this.http.get<Config>('http://localhost:2000');
    // 不订阅 是不会发送请求
    httpClient.subscribe(
      (data) => {
        console.log(data);
      },
      error => {
        console.error(error);
      }
    );
    return httpClient;
}

}
