import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
export interface ActionData {
  action: string | 'edit-home' | 'switch-menu';
  data?: any;
}
@Injectable({
  providedIn: 'root'
})

export class ObserveSubjectService {
  actionSubject = new Subject<any>();
  constructor() { }
  dispatchGeneralAction(action: 'edit-home' | 'switch-menu'|'home-setting', data?) {
    this.actionSubject.next({ action, data });
  }

  // dispatchPayloadAction(action: actionType, data: any) {
  //   const newAction = new PayloadAction(action, data);
  //   this.actionSubject.next(newAction);
  // }

  unsubscribeSubject() {
    this.actionSubject.unsubscribe();
  }
}
