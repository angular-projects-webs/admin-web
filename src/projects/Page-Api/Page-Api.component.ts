import { Component, OnInit } from '@angular/core';
import { PageApiBizService } from './page-api-biz.service';
import { PageAPI, UIModel } from './page-api.state';

@Component({
  selector: 'admin-app-page-api',
  templateUrl: './Page-Api.component.html',
  styleUrls: ['./Page-Api.component.scss'],
  providers: [PageApiBizService]
})
export class PageApiComponent implements OnInit {
  isVisibleNzModal = false;
  action: 'Add' | 'Update' | 'Delete' | null;
  UIModel = new UIModel();

  constructor(private bizSvc: PageApiBizService) { }

  ngOnInit() {
    this.bizSvc.setCompnent(this);
    this.bizSvc.resetInit();
  }

  onQuery(event) {

  }

  onCopyCreate(value: PageAPI) {
    this.bizSvc.create(value);
  }

  onUpdate(value: PageAPI) {
    this.bizSvc.update(value);
  }

  onDelete(value: PageAPI) {
    this.bizSvc.delete(value);
  }



  onCreate() {
    this.bizSvc.create();
  }

  onSave() {
    this.bizSvc.save();
  }

  onCancel() {
    this.isVisibleNzModal = false;
  }

  ngChangeAPP(event: PageAPI) {
    this.UIModel.maintion.node_id = event.node_id;
    this.UIModel.maintion.node_name = event.node_name;
  }

  ngChangeAPI(event: PageAPI) {
    this.UIModel.maintion.prog_id = event.prog_id;
    this.UIModel.maintion.prog_name = event.prog_name;
  }

}
