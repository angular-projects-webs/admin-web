import { MenuTable } from '../menu-management/menu-management.state';
import { ProgramItem } from '../program-list/program-list.state';


// Page 一个来自Progam List 或者来自 menu
export class PageAPI extends MenuTable {
  prog_id: string;
  prog_name: string;
  prog_type: string; // `APP` | `API`;
  url_path: string;
  img_path?: string;
  background_Color?: string;

  rowSpan?: number; // 呈现时用
}

export interface ItemOfColumn {
  key: string;
  title: string;
  nzWidth: string;
  nzAlign: 'left' | 'right' | 'center';
  compare?: any; // null | Function;
  priority?: number;
}


export const ListOfColumn: ItemOfColumn[] = [
  {
    key: 'node_name',
    title: 'Page Name',
    compare: (a: PageAPI, b: PageAPI) => a.node_name.localeCompare(b.node_name),
    priority: 1,
    nzWidth: '300px',
    nzAlign: 'left'
  },
  {
    key: 'node_id',
    title: 'Page Id',
    compare: (a: PageAPI, b: PageAPI) => a.node_id.localeCompare(b.node_id),
    priority: 1,
    nzWidth: '200px',
    nzAlign: 'left',
  },
  {
    key: 'prog_id',
    title: 'API Id',
    compare: (a: PageAPI, b: PageAPI) => a.prog_id.localeCompare(b.prog_id),
    priority: 1,
    nzWidth: '130px',
    nzAlign: 'center'
  },
  {
    key: 'prog_name',
    title: 'API Name',
    compare: (a: PageAPI, b: PageAPI) => a.prog_name.localeCompare(b.prog_name),
    priority: 1,
    nzWidth: '200px',
    nzAlign: 'left'
  },
  {
    key: 'url_path',
    title: 'Url',
    compare: (a: PageAPI, b: PageAPI) => a.url_path.localeCompare(b.url_path),
    priority: 1,
    nzWidth: '',
    nzAlign: 'left'
  }
  // { // actions

  // }
];
export class Maintion extends PageAPI {
  APP: PageAPI;
  API: ProgramItem;
}
export class UIModel {
  programApiListS:ProgramItem[] = [];
  programApiList: ProgramItem[] = [];
  progamGroupByNodeId = {};
  pageList: PageAPI[] = [];

  listOfColumn: Array<ItemOfColumn> = ListOfColumn;
  listOfData: PageAPI[] = [];

  maintion: Maintion = new Maintion();
}
