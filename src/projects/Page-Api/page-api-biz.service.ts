import { Injectable } from '@angular/core';
import { TESTMENUDATA } from '../menu-management/menu-management.state';
import { ProgamList, ProgramItem } from '../program-list/program-list.state';
import { PageApiComponent } from './page-api.component';

import * as _ from 'lodash';
import { Maintion, PageAPI } from './page-api.state';

@Injectable()
export class PageApiBizService {
  private component: PageApiComponent;
  constructor() { }
  setCompnent(component: PageApiComponent) {
    this.component = component;
  }

  resetInit() {
    const { UIModel } = this.component;
    UIModel.programApiListS = _.filter(ProgamList, value => value.prog_type === 'API');
    UIModel.programApiList = _.cloneDeep(UIModel.programApiListS);
    UIModel.pageList = _.filter(TESTMENUDATA, value => value.node_type === '1');
    this.resetListOfData();
  }

  resetListOfData() {
    const { UIModel } = this.component;
    UIModel.progamGroupByNodeId = _.groupBy(UIModel.programApiList, 'node_id');
    UIModel.listOfData = [];
    _.each(UIModel.pageList, (value: PageAPI) => {
      const list: ProgramItem[] = UIModel.progamGroupByNodeId[value.node_id];
      if (list) {
        _.each(list, (valueProg, index) => UIModel.listOfData.push({ ...value, ...valueProg, rowSpan: index === 0 ? list.length : 0 }));
      } else {
        UIModel.listOfData.push({ ...value, prog_id: '', rowSpan: 1 });
      }
    });
  }

  setMaintion(item: PageAPI) {
    const { UIModel } = this.component;
    UIModel.maintion = new Maintion();
    if (item) {
      UIModel.maintion = { ...UIModel.maintion, ...item };
      UIModel.maintion.APP = UIModel.pageList.find(value => value.node_id === item.node_id);
      UIModel.maintion.API = UIModel.programApiListS.find(value => value.prog_id === item.prog_id);
    }
    this.component.isVisibleNzModal = true;
  }

  create(item?: PageAPI) {
    this.component.action = 'Add';
    const { UIModel } = this.component;
    this.setMaintion(item);
  }

  update(item: PageAPI) {
    this.component.action = 'Update';
    this.setMaintion(item);
  }

  delete(item: PageAPI) {
    const { UIModel } = this.component;
    const success = () => {
      _.remove(UIModel.programApiList, value => (value.node_id === item.node_id &&  value.prog_id === item.prog_id));
      this.resetListOfData();
    };
    success();
  }

  save() {
    const { UIModel } = this.component;
    if (this.component.action === 'Add') {
      const success = () => {
        UIModel.programApiList.push(_.pick(UIModel.maintion, ['prog_id', 'prog_name', 'prog_type', 'url_path', 'node_id']));
        this.resetListOfData();
        this.component.isVisibleNzModal = false;
      };
    } else {

    }

  }

}
