import { Component, OnInit } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observable, Subject } from 'rxjs';
import { ProgramListBizService } from './program-list-biz.service';
import { UIModel } from './program-list.state';
@Component({
  selector: 'admin-app-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: ['./program-list.component.scss'],
  providers: [ProgramListBizService]
})
export class ProgramListComponent implements OnInit {
  isVisibleNzModal = false;
  testImg = 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png';
  selectedProvince = 'APP';
  UIModel = new UIModel();


  fileList: NzUploadFile[] = [
    // {
    //   uid: '-1',
    //   name: 'image.png',
    //   status: 'done',
    //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    // },
    // {
    //   uid: '-2',
    //   name: 'image.png',
    //   status: 'done',
    //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    // },
    // {
    //   uid: '-3',
    //   name: 'image.png',
    //   status: 'done',
    //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    // },
    // {
    //   uid: '-4',
    //   name: 'image.png',
    //   status: 'done',
    //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    // },
    // {
    //   uid: '-xxx',
    //   percent: 50,
    //   name: 'image.png',
    //   status: 'uploading',
    //   url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
    // },
    // {
    //   uid: '-5',
    //   name: 'image.png',
    //   status: 'error'
    // }
  ];
  previewImage: string | undefined = '';
  previewVisible = false;

  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      // file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  }
  stopwatchValue$: Subject<boolean> = new Subject();
  confirm = () => {
    this.stopwatchValue$.next(true);
    return true;
  }
  onCancel = () => {
    this.stopwatchValue$.next(false);
    return false;
  }
  // observable = Observable<boolean>
  i = 0;
  nzBeforeUpload = (file: NzUploadFile, fileList: NzUploadFile[]) => {
    // const node = document.getElementById('sss');
    // node.click();
    // this.i ++;
    // return  new Observable((f)=> f.next( this.i ===3));
  }


  constructor(private bizSvc: ProgramListBizService) {

  }


  ngOnInit(): void {
    let i = 0;
    while (i < 30) {
      this.UIModel.listOfData.push(
        {
          prog_id: `${i}`.padStart(6, '0'),
          prog_name: `prog_name${i}`,
          prog_type: (i % 2) === 0 ? 'APP' : 'API',
          url_path: '',
          img_path: 'http://localhost/pictures/zhuque.png',
          background_Color: 'red'
        });
      i++;
    }

    this.stopwatchValue$.subscribe(num => {
      console.log(num);
    });
  }

  onQuery(value: string) {
    console.log(value);

  }

  onUploadList() {
    this.bizSvc.onUploadList();
  }

  onCreate() {
    this.isVisibleNzModal = true;
  }

  onEditCancel() {
    this.isVisibleNzModal = false;
  }
  onEditSave() {

  }

  nzUploadPicture($event) {
    const { file, fileList, event } = $event;
  }

}
