import { Injectable } from '@angular/core';
import { ApiService } from 'src/common/service';

@Injectable()
export class ProgramListBizService {
  constructor(private apiService: ApiService) {
  }

  onUploadList() {
    this.apiService.get();
  }

}
