export const ProgamList: ProgramItem[] = [
  {
    node_id: '2021-09-23-21:52-122',
    prog_id: `${1}`.padStart(6, '0'),
    prog_name: `Get Menu`,
    prog_type: 'API',
    url_path: '',
    img_path: 'http://localhost/pictures/zhuque.png',
    background_Color: 'red'
  },
  {
    node_id: '2021-09-23-21:52-122',
    prog_id: `${2}`.padStart(6, '0'),
    prog_name: `Get Menu`,
    prog_type: 'API',
    url_path: '',
    img_path: 'http://localhost/pictures/zhuque.png',
    background_Color: 'red'
  },
  {
    node_id: '2021-09-23-21:52-122',
    prog_id: `${3}`.padStart(6, '0'),
    prog_name: `Get Menu`,
    prog_type: 'API',
    url_path: '',
    img_path: 'http://localhost/pictures/zhuque.png',
    background_Color: 'red'
  }
];

export class ProgramItem {
  prog_id: string;
  prog_name: string;
  prog_type: string; // `APP` | `API`;
  url_path: string;
  img_path: string;
  background_Color?: string;

  node_id?: string; // 关联 menu
}

export interface ItemOfColumn {
  key: string;
  title: string;
  nzWidth: string;
  nzAlign: 'left' | 'right' | 'center';
  compare?: any; // null | Function;
  priority?: number;
}

export const ListOfColumn: ItemOfColumn[] = [
  {
    key: 'prog_id',
    title: 'Program ID',
    compare: (a: ProgramItem, b: ProgramItem) => a.prog_id.localeCompare(b.prog_id),
    priority: 1,
    nzWidth: '120px',
    nzAlign: 'left',
  },
  {
    key: 'prog_name',
    title: 'Program Name',
    compare: (a: ProgramItem, b: ProgramItem) => a.prog_name.localeCompare(b.prog_name),
    priority: 1,
    nzWidth: '180px',
    nzAlign: 'left'
  },
  {
    key: 'prog_type',
    title: 'Program Type',
    compare: (a: ProgramItem, b: ProgramItem) => a.prog_type.localeCompare(b.prog_type),
    priority: 1,
    nzWidth: '130px',
    nzAlign: 'center'
  },
  {
    key: 'url_path',
    title: 'Url Path',
    compare: (a: ProgramItem, b: ProgramItem) => a.url_path.localeCompare(b.url_path),
    priority: 1,
    nzWidth: '',
    nzAlign: 'left'
  },
  {
    key: 'background_Color',
    title: 'Background Color',
    compare: (a: ProgramItem, b: ProgramItem) => a.background_Color.localeCompare(b.background_Color),
    priority: 1,
    nzWidth: '180px',
    nzAlign: 'center'
  },
  {
    key: 'img_path',
    title: 'Img Path',
    compare: (a: ProgramItem, b: ProgramItem) => a.img_path.localeCompare(b.img_path),
    priority: 1,
    nzWidth: '300px',
    nzAlign: 'left'
  },
];
export class UIModel {
  listOfColumn: Array<ItemOfColumn> = ListOfColumn;
  listOfData: ProgramItem[] = [];

}
