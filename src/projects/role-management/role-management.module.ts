import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleManagementComponent } from './role-management.component';

import { NzTableModule } from 'ng-zorro-antd/table';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

import { QueryModule } from '../components/query/query.module';
import { CreateUpdateDeleteModule } from '../components/create-update-delete/create-update-delete.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NzTableModule,
    QueryModule,
    CreateUpdateDeleteModule,
    NzImageModule,
    NzButtonModule,
    NzIconModule,
    NzModalModule,
    NzInputModule,
    NzUploadModule,
    NzSelectModule,
    NzPopconfirmModule,
    NzMessageModule,
    NzCollapseModule,
    NzCheckboxModule
  ],
  declarations: [RoleManagementComponent]
})
export class RoleManagementModule { }
