import { Component, OnInit } from '@angular/core';
import { RoleManagementBizService } from './role-management-biz.service';
import { UIModel } from './role-management.state';

@Component({
  selector: 'admin-app-role-management',
  templateUrl: './role-management.component.html',
  styleUrls: ['./role-management.component.scss'],
  providers: [RoleManagementBizService]
})
export class RoleManagementComponent implements OnInit {
  isVisibleNzModal = false;
  action: 'Add' | 'Update' | 'Delete';
  UIModel = new UIModel();
  constructor( private bizSvc: RoleManagementBizService) { }

  ngOnInit() {
    this.bizSvc.setComponent(this);
    this.bizSvc.initUIData();
  }

  onQuery(event) {

  }

  onCreate() {
    this.bizSvc.create();
  }


  onCopyCreate(data) {
    this.bizSvc.create(data);
  }

  onUpdate(data) {
    this.bizSvc.update(data);
  }

  onDelete(data) {
    this.bizSvc.delete(data);
  }

  handleCancel() {
    this.isVisibleNzModal = false;
  }

  handleOk() {
    this.bizSvc.save();
  }

}
