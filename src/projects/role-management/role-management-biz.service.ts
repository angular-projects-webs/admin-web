import { Injectable } from '@angular/core';
import { RoleManagementComponent } from './role-management.component';
import { Maintain, RoleShow, RoleTable, ROLETEST } from './role-management.state';
import { NzMessageService } from 'ng-zorro-antd/message';

import * as _ from 'lodash';
import * as moment from 'moment';
import * as utils from '../../common/utils';
@Injectable()
export class RoleManagementBizService {

  private component: RoleManagementComponent;
  constructor(private nzMessageSvc: NzMessageService) { }

  setComponent(component: RoleManagementComponent) {
    this.component = component;
  }

  initUIData() {
    const { UIModel } = this.component;
    UIModel.RoleTableS = ROLETEST;
    this.resetUI();
  }

  resetUI() {
    const { UIModel } = this.component;
    const groupByRoleId = _.groupBy(UIModel.RoleTableS, 'role_id');
    UIModel.listOfData = [];
    _.each(groupByRoleId, (value: RoleTable[]) => {
      const groupByProgType = _.groupBy(value, 'prog_type');
      const roleShow = { ...value[0], ...groupByProgType };
      UIModel.listOfData.push(roleShow);
    });
  }


  setMiantain(item: RoleShow) {
    const { UIModel } = this.component;
    UIModel.maintain = new Maintain();
    if (item) {
      UIModel.maintain = { ...item };
    }
  }

  create(item?) {
    this.component.action = 'Add';
    this.setMiantain(item);
    this.component.isVisibleNzModal = true;
  }

  update(item) {
    this.component.action = 'Update';
    this.setMiantain(item);
    this.component.isVisibleNzModal = true;
  }

  delete(item) {
    this.component.action = 'Delete';
    this.setMiantain(item);
    const success = () => {
      const { UIModel: { maintain, listOfData } } = this.component;
      const { role_id } = maintain;
      _.remove(listOfData, { role_id });
      this.component.UIModel.listOfData = _.cloneDeep(listOfData);
    };
    success();
  }

  save() {
    const { UIModel } = this.component;
    const { maintain, listOfData } = UIModel;
    if (!utils.checkObjct(maintain, ['role_name'])) {
      this.nzMessageSvc.warning(`The * required fields!`);
      return;
    }
    if (!maintain.role_id) {
      maintain.role_id = `ROLE${moment().format(`YYYYMMDDHHmmSSS`)}`;
    }
    const success = () => {
      // const index = listOfData.findIndex(value => value.user_id === maintain.user_id);
      // 需要添加检测是否已经存在同名的
      if (this.component.action === 'Add') {
        listOfData.unshift(maintain);
      } else {
        // listOfData.splice(index, 1, maintain);
      }
      UIModel.listOfData = _.cloneDeep(listOfData);
      this.component.isVisibleNzModal = false;
    };
    success();
  }

}
