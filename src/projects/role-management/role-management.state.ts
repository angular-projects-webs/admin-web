export const ROLETEST: RoleTable[] = [
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0000',
    prog_type: 'APP',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  },
  {
    role_id: 'Role_Id0001',
    role_name: 'Admin',
    role_description: '管理员',
    prog_id: '0002',
    prog_type: 'API',
    read_auth: true,
    create_auth: true,
    update_auth: true,
    delete_auth: true,
  }
];


export interface ItemOfColumn {
  key: string;
  title: string;
  nzWidth: string;
  nzAlign: 'left' | 'right' | 'center';
  compare?: any; // null | Function;
  priority?: number;
}


export const ListOfColumn: ItemOfColumn[] = [
  {
    key: 'role_id',
    title: 'Role Id',
    compare: (a: RoleTable, b: RoleTable) => a.role_id.localeCompare(b.role_id),
    priority: 1,
    nzWidth: '200px',
    nzAlign: 'center'
  },
  {
    key: 'role_name',
    title: 'Role Name',
    compare: (a: RoleTable, b: RoleTable) => a.role_name.localeCompare(b.role_name),
    priority: 1,
    nzWidth: '250px',
    nzAlign: 'left',
  },
  {
    key: 'role_description',
    title: 'Role Description',
    compare: (a: RoleTable, b: RoleTable) => a.role_description.localeCompare(b.role_description),
    priority: 1,
    nzWidth: '250px',
    nzAlign: 'left'
  },
  {
    key: 'prog_list',
    title: 'APP && API Auth',
    compare: null,
    priority: 1,
    nzWidth: '',
    nzAlign: 'center'
  }
  // { // actions

  // }
];




export class RoleTable {
  role_id: string;
  role_name: string;
  role_description: string;
  prog_id: string;
  prog_type: 'APP' | 'API';
  read_auth: boolean;
  create_auth: boolean;
  update_auth: boolean;
  delete_auth: boolean;
}

export class RoleShow extends RoleTable {
  APP?: RoleTable[];
  API?: RoleTable[];
}

export class Maintain extends RoleShow{

}

export class UIModel {
  listOfColumn: Array<ItemOfColumn> = ListOfColumn;
  RoleTableS: RoleTable[] = [];
  APPlistS = [];
  APIListS = [];
  listOfData: RoleShow[] = [];
  maintain: Maintain = new Maintain();
}
