import { NgModule } from '@angular/core';
import { BaseDataManagementModule } from './Base-data-management/Base-data-management.module';
import { MenuManagementModule } from './menu-management/menu-management.module';
import { PageApiModule } from './page-api/page-api.module';
import { ProgramListModule } from './program-list/program-list.module';
import { RoleManagementModule } from './role-management/role-management.module';
import { UserManagementModule } from './user-management/user-management.module';
@NgModule({
  imports: [
    BaseDataManagementModule,
    MenuManagementModule,
    PageApiModule,
    ProgramListModule,
    RoleManagementModule,
    UserManagementModule
  ]
})
export class ProjectsModule { }
