import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'admin-app-create-update-delete',
  templateUrl: './create-update-delete.component.html',
  styleUrls: ['./create-update-delete.component.scss']
})
export class CreateUpdateDeleteComponent {
  disableObj = {
    c: false,
    u: false,
    d: false
  };

  @Input() cLoading = false;
  @Input() uLoading = false;
  @Input() dLoading = false;
  @Input() set disable(value) {
    this.disableObj = { ...this.disableObj, ...value };
  }

  @Output() create = new EventEmitter<any>();
  @Output() update = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();

  onCreate() {
    this.create.emit();
  }
  onUpdate() {
    this.update.emit();
  }
  onDeleteConfirm() {
    this.delete.emit();
  }

}
