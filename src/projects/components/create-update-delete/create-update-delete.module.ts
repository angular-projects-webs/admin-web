import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUpdateDeleteComponent } from './create-update-delete.component';

import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
@NgModule({
  imports: [
    CommonModule,
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule
  ],
  exports: [CreateUpdateDeleteComponent],
  declarations: [CreateUpdateDeleteComponent]
})
export class CreateUpdateDeleteModule { }
