import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'admin-app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.scss']
})
export class QueryComponent implements OnInit {
  value = '';
  @Output() query = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onQuery(event: Event) {

    this.query.emit(this.value);
    event.stopPropagation();
  }

}
