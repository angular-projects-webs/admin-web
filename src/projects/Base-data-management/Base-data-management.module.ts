import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseDataManagementComponent } from './Base-data-management.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BaseDataManagementComponent]
})
export class BaseDataManagementModule { }
