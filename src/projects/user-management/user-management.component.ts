import { Component, OnInit } from '@angular/core';
import { UserManagementBizService } from './user-management-biz.service';
import { UIModel } from './user-management.state';

@Component({
  selector: 'admin-app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
  providers: [UserManagementBizService]
})
export class UserManagementComponent implements OnInit {
  isVisibleNzModal = false;
  action: 'Add' | 'Update' | 'Delete';
  UIModel = new UIModel();

  constructor( private bizSvc: UserManagementBizService) { }

  ngOnInit() {
    this.bizSvc.setComponent(this);
    this.bizSvc.initUIData();
  }
  onQuery(event) {

  }

  onCreate() {
    this.bizSvc.create();
  }


  onCopyCreate(data) {
    this.bizSvc.create(data);
  }

  onUpdate(data) {
    this.bizSvc.update(data);
  }

  onDelete(data) {
    this.bizSvc.delete(data);
  }

  handleCancel() {
    this.isVisibleNzModal = false;
  }

  handleOk() {
    this.bizSvc.save();
  }

}
