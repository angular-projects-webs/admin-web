import { Injectable } from '@angular/core';
import { UserManagementComponent } from './user-management.component';
import { Maintain, UserTable, USERTEST } from './user-management.state';
import { NzMessageService } from 'ng-zorro-antd/message';

import * as _ from 'lodash';
import * as utils from '../../common/utils';


@Injectable()
export class UserManagementBizService {
  private component: UserManagementComponent;
  constructor(private nzMessageSvc: NzMessageService) { }

  setComponent(component: UserManagementComponent) {
    this.component = component;
  }

  initUIData() {
    const { UIModel } = this.component;
    UIModel.listOfData = USERTEST;
  }

  setMiantain(item: UserTable) {
    const { UIModel } = this.component;
    UIModel.maintain = new Maintain();
    if (item) {
      UIModel.maintain = { ...item };
    }
  }

  create(item?) {
    this.component.action = 'Add';
    this.setMiantain(item);
    this.component.isVisibleNzModal = true;
  }

  update(item) {
    this.component.action = 'Update';
    this.setMiantain(item);
    this.component.isVisibleNzModal = true;
  }

  delete(item) {
    this.component.action = 'Delete';
    this.setMiantain(item);
    const success = () => {
      let { UIModel: { maintain, listOfData } } = this.component;
      const { user_id } = maintain;
      _.remove(listOfData, { user_id });
      this.component.UIModel.listOfData = _.cloneDeep(listOfData);
    };
    success();
  }

  save() {
    const { UIModel } = this.component;
    const { maintain, listOfData } = UIModel;
    if (!utils.checkObjct(maintain, ['user_id', 'user_name', 'user_phone', 'user_email'])) {
      this.nzMessageSvc.warning(`The * required fields!`);
      return;
    }
    if (!/^1\d{10}/.test(maintain.user_phone)) {
      this.nzMessageSvc.warning(`手机号码 格式有问题`);
      return;
    }
    const success = () => {
      const index = listOfData.findIndex(value => value.user_id === maintain.user_id);
      if (this.component.action === 'Add') {
        listOfData.unshift(maintain);
      } else {
        listOfData.splice(index, 1, maintain);
      }
      UIModel.listOfData = _.cloneDeep(listOfData);
      this.component.isVisibleNzModal = false;
    };
    success();
  }
}
