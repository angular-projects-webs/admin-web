export const USERTEST: UserTable[] = [
  {
    user_id: 'Z19070517',
    user_name: 'ZHOU MOU',
    user_name_zh: '周海峰',
    user_phone: '18878395397',
    user_email: '1557111209@qq.com',
    user_password: '',
    role: '',
    base_data: '',
    state: 'Enable',
  },
  {
    user_id: 'Z19070518',
    user_name: 'ZHOU MOU',
    user_name_zh: '周海峰',
    user_phone: '18878395397',
    user_email: '1557111209@qq.com',
    user_password: '',
    role: '',
    base_data: '',
    state: 'Enable',
  }
];

export class UserTable {
  user_id: string;
  user_name: string;
  user_name_zh: string;
  user_phone: string;
  user_email: string;
  user_password?: string;
  state?: string;
  role?: string;
  base_data?: string;
}

export class Maintain extends UserTable{
  role?: string;
  base_data?: string;
}
export interface ItemOfColumn {
  key: string;
  title: string;
  nzWidth: string;
  nzAlign: 'left' | 'right' | 'center';
  compare?: any; // null | Function;
  priority?: number;
}


export const ListOfColumn: ItemOfColumn[] = [
  {
    key: 'user_id',
    title: '账号',
    compare: (a: UserTable, b: UserTable) => a.user_id.localeCompare(b.user_id),
    priority: 1,
    nzWidth: '150px',
    nzAlign: 'center'
  },
  {
    key: 'user_name',
    title: '用户名',
    compare: (a: UserTable, b: UserTable) => a.user_name.localeCompare(b.user_name),
    priority: 1,
    nzWidth: '200px',
    nzAlign: 'left',
  },
  {
    key: 'user_name_zh',
    title: '姓名',
    compare: (a: UserTable, b: UserTable) => a.user_name_zh.localeCompare(b.user_name_zh),
    priority: 1,
    nzWidth: '200px',
    nzAlign: 'center'
  },
  {
    key: 'user_phone',
    title: '手机号',
    compare: (a: UserTable, b: UserTable) => a.user_phone.localeCompare(b.user_phone),
    priority: 1,
    nzWidth: '200px',
    nzAlign: 'center'
  },
  {
    key: 'user_email',
    title: '邮箱',
    compare: (a: UserTable, b: UserTable) => a.user_email.localeCompare(b.user_email),
    priority: 1,
    nzWidth: '',
    nzAlign: 'left'
  },
  {
    key: 'state',
    title: '状态',
    compare: (a: UserTable, b: UserTable) => a.state.localeCompare(b.state),
    priority: 1,
    nzWidth: '120px',
    nzAlign: 'center'
  }
  // { // actions

  // }
];

export class UIModel {
  listOfColumn: Array<ItemOfColumn> = ListOfColumn;
  listOfData: UserTable[] = [];
  listRoleS = [];
  listBaseDs = [];
  maintain: Maintain = new Maintain();
}
