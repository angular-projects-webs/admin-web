import { Component, OnInit } from '@angular/core';
import { ProgramItem } from '../program-list/program-list.state';
import { MenuManagementBizService } from './menu-management-biz.service';
import { TreeNode, UIModel } from './menu-management.state';

@Component({
  selector: 'admin-app-menu-management',
  templateUrl: './menu-management.component.html',
  styleUrls: ['./menu-management.component.scss'],
  providers: [MenuManagementBizService]
})
export class MenuManagementComponent implements OnInit {
  nzModalIsVisible = false;
  action: 'Add' | 'Update' | 'Delete' | null;
  UIModel = new UIModel();

  constructor(private bizSvc: MenuManagementBizService) {
  }

  ngOnInit() {
    this.bizSvc.setComponent(this);
    this.bizSvc.resetInit();
  }

  collapse(array: TreeNode[], data: TreeNode, $event: boolean): void {
    if (!$event) {
      if (data.children) {
        data.children.forEach(d => {
          const target = array.find(a => a.key === d.key)!;
          target.expand = false;
          this.collapse(array, target, false);
        });
      } else {
        return;
      }
    }
  }

  ngModelChangeProg(event: ProgramItem) {
    if (event) {
      this.UIModel.maintionNode.node_type = '1';
      this.UIModel.maintionNode.node_name = event.prog_name;
      this.UIModel.maintionNode.prog_id = event.prog_id;
    } else {
      this.UIModel.maintionNode.node_type = '0';
    }

  }

  onCreate() {
    this.bizSvc.create();
  }

  onCopyCreate(item: TreeNode) {
    this.bizSvc.create(item);
  }

  onUpdate(item: TreeNode) {
    this.bizSvc.update(item);
  }
  onDelete(item) {
    this.bizSvc.delete(item);
  }

  handleCancel() {
    this.nzModalIsVisible = false;
  }

  handleOk() {
    this.bizSvc.maintainMenu();
  }


}
