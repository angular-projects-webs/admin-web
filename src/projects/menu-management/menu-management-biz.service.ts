import { Injectable } from '@angular/core';
import { MenuManagementComponent } from './menu-management.component';
import { MenuTable, ProgList, TESTMENUDATA, TreeNode } from './menu-management.state';
import { NzMessageService } from 'ng-zorro-antd/message';

import * as _ from 'lodash';
import * as moment from 'moment';
import * as utils from '../../common/utils';
@Injectable()
export class MenuManagementBizService {
  private component: MenuManagementComponent;
  constructor(private nzMessageSvc: NzMessageService) { }

  setComponent(component: MenuManagementComponent) {
    this.component = component;
  }

  resetInit() {
    const { UIModel } = this.component;
    UIModel.menulist = TESTMENUDATA;
    UIModel.groupMap = _.groupBy(UIModel.menulist, 'parent_node_id');
    UIModel.listOfMapData = this.getChildren(UIModel.groupMap, '0000-00-00-00:00-000', '0');
    UIModel.listOfMapData.forEach(item => {
      UIModel.mapOfExpandedData[item.key] = this.convertTreeToList(item);
    });
    UIModel.progamList = ProgList;
  }

  getChildren(groupMap, nodeId, key: string): TreeNode[] {
    const children: TreeNode[] = [];
    const list: MenuTable[] = _.orderBy(groupMap[nodeId], ['node_order']);
    _.each(list, (value: MenuTable, index) => {
      const currentKey = `${key}-${index}`;
      let nextChildren: TreeNode[] = [];
      let node: TreeNode = { ...value, key: currentKey };
      if (value.node_type === '0') {
        nextChildren = this.getChildren(groupMap, value.node_id, currentKey);
        node = { ...node, children: nextChildren };
        children.push(node);
      } else {
        children.push(node);
      }
    });
    return children;
  }

  getNodes(nodeId, exclude?: string): TreeNode[] {
    const { UIModel: { groupMap } } = this.component;
    const children: TreeNode[] = [];
    const list: MenuTable[] = _.orderBy(groupMap[nodeId], ['node_order']);
    _.each(list, (value: MenuTable, index) => {
      let node: TreeNode = { ...value, key: value.node_id, title: value.node_name };
      if (value.node_type === '0' && value.node_id !== exclude) {
        const nextChildren = this.getNodes(value.node_id, exclude);
        node = { ...node, children: nextChildren };
        children.push(node);
      }
    });
    return children;

  }

  convertTreeToList(root: TreeNode): TreeNode[] {
    const stack: TreeNode[] = [];
    const array: TreeNode[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });

    while (stack.length !== 0) {
      const node = stack.pop();
      this.visitNode(node, hashMap, array);
      if (node.children) {
        for (let i = node.children.length - 1; i >= 0; i--) {
          stack.push({ ...node.children[i], level: node.level + 1, expand: false, parent: node });
        }
      }
    }

    return array;
  }

  visitNode(node: TreeNode, hashMap: { [key: string]: boolean }, array: TreeNode[]): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }

  create(item?: TreeNode) {
    const { UIModel } = this.component;
    this.component.action = 'Add';
    // Init selected Tree nodes
    UIModel.nodes[0].children = this.getNodes('0000-00-00-00:00-000');
    UIModel.maintionNode = new TreeNode();
    if (item) {
      UIModel.maintionNode = { ...item };
    }
    UIModel.maintionNode.node_id = moment().format(`YYYY-MM-DD-HH:mm-SSS`);
    this.component.nzModalIsVisible = true;

  }

  update(item: TreeNode) {
    const { UIModel } = this.component;
    this.component.action = 'Update';
    UIModel.maintionNode = { ...item };
    this.component.nzModalIsVisible = true;
  }

  delete(item: TreeNode) {
    const { UIModel } = this.component;
    this.component.action = 'Delete';
    UIModel.maintionNode = { ...item };
  }

  maintainMenu() {
    const { UIModel } = this.component;
    if (utils.checkObjct(UIModel.maintionNode, ['parent_node_id', 'node_name'])) {
      UIModel.maintionNode.node_order = (UIModel.groupMap[UIModel.maintionNode.parent_node_id] || []).length;
    } else {
      this.nzMessageSvc.warning(`The * required fields!`);
    }
  }

}
