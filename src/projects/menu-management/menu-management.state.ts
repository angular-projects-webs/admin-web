import { ProgramItem } from "../program-list/program-list.state";

export const TESTMENUDATA = [
  {
    node_id: '2021-09-23-21:52-120',
    node_order: 0,
    node_name: 'Admin',
    parent_node_id: '0000-00-00-00:00-000',
    node_type: '0',
    prog_id: null,
  },
  {
    node_id: '2021-09-23-21:52-122',
    node_name: 'Menu Mangement',
    node_order: 1,
    parent_node_id: '2021-09-23-21:52-120',
    node_type: '1',
    prog_id: '0002',
  },
  {
    node_id: '2021-09-23-21:52-123',
    node_name: 'Page & API',
    node_order: 0,
    parent_node_id: '2021-09-23-21:52-120',
    node_type: '1',
    prog_id: '0003',
  },
  {
    node_id: '2021-09-23-21:52-124',
    node_order: 1,
    node_name: 'Hello',
    parent_node_id: '0000-00-00-00:00-000',
    node_type: '0',
    prog_id: null,
  },
];

export const ProgList = [
  {
    prog_id: '0001',
    prog_name: 'Menu Name',
    prog_type: `APP`,
    url_path: '',
    img_path: '',
    background_Color: '',
  },
  {
    prog_id: '0002',
    prog_name: 'Menu Name',
    prog_type: `APP`,
    url_path: '',
    img_path: '',
    background_Color: '',
  },
  {
    prog_id: '0003',
    prog_name: 'Menu Name',
    prog_type: `APP`,
    url_path: '',
    img_path: '',
    background_Color: '',
  }
];

export class MenuTable {
  node_id: string;  //
  node_order: number;
  node_name: string;
  node_type: string = '0';
  parent_node_id: string;
  prog_id: string | null;
}


export class TreeNode extends MenuTable {
  key: string;      // 唯一 group_id
  children?: TreeNode[];
  parent?: TreeNode;
  level?: number;   // 间距
  expand?: boolean; // 是否展开

  title?: string;    // node_name: string;
}

export class UIModel {
  menulist: Array<MenuTable> = [];
  groupMap: {};
  listOfMapData: TreeNode[] = [];
  mapOfExpandedData: { [key: string]: TreeNode[] } = {};
  progamList: ProgramItem[] = [];
  nodes: TreeNode[] = [
    {
      ...(new MenuTable()),
      key: '0000-00-00-00:00-000',
      title: 'Menu Root'
    }
  ];
  nzExpandedKeys = [];	 //默认展开指定的树节点

  maintionNode: TreeNode;

  selectedValue = {};
}
